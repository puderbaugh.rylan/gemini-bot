package config

import (
	"os"
	"strings"
)

type Config struct {
	DiscordToken  string
	CommandPrefix string
}

var cfg *Config

func Load() {
	cfg = &Config{
		DiscordToken:  strings.TrimSpace(os.Getenv("DISCORD_BOT_TOKEN")),
		CommandPrefix: strings.TrimSpace(os.Getenv("COMMAND_PREFIX")),
	}

	if len(cfg.DiscordToken) == 0 {
		panic("The environment variable 'DISCORD_BOT_TOKEN' must not be empty!")
	}
	if len(cfg.CommandPrefix) != 1 {
		cfg.CommandPrefix = "!"
	}
}

func Get() *Config {
	return cfg
}
