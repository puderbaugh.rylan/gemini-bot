package main

import (
	"context"
	"discord-bot/config"
	"fmt"
	"github.com/bwmarrin/discordgo"
	"github.com/google/generative-ai-go/genai"
	"google.golang.org/api/option"
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

func main() {
	log.Printf("Starting Application")
	config.Load()

	// Create Discord Bot Session
	bot, err := discordgo.New("Bot " + config.Get().DiscordToken)
	if err != nil {
		fmt.Println("Error creating Discord session,", err)
		println("err")
		panic(err)
	}

	// Safely close down the Discord session
	defer func(bot *discordgo.Session) {
		err := bot.Close()
		if err != nil {

		}
	}(bot)
	defer func() {
		log.Printf("Shutting Down")
		time.Sleep(1 * time.Second)
	}()

	// Register the handleMessage func
	bot.AddHandler(handleMessages)

	// Identify necessary bot intents
	bot.Identify.Intents = discordgo.IntentsGuildMessages

	// Open a websocket connection to Discord.
	err = bot.Open()
	if err != nil {
		log.Println("Error Connecting!", err)
		return
	}

	_ = bot.UpdateCustomStatus("Responding to your request")

	log.Println("Successfully Connected!")
	log.Println("Command Prefix is = " + config.Get().CommandPrefix)

	command := &discordgo.ApplicationCommand{
		Name:        "ping",
		Description: "Replies with Pong!",
	}

	// Register the command with Discord
	_, err = bot.ApplicationCommandCreate(bot.State.User.ID, "", command)
	if err != nil {
		fmt.Println("Error creating command:", err)
		return
	}

	// Wait till the bot is killed
	channel := make(chan os.Signal, 1)
	signal.Notify(channel, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-channel

	log.Println("Terminating Bot")
}

func handleMessages(bot *discordgo.Session, message *discordgo.MessageCreate) {
	var commandPrefix = config.Get().CommandPrefix

	messagePortions := strings.Split(message.Content, " ")

	if messagePortions[0] == commandPrefix+"task" {
		ctx := context.Background()
		// Access your API key as an environment variable
		client, err := genai.NewClient(ctx, option.WithAPIKey(os.Getenv("GEMINI_API_KEY")))
		if err != nil {
			log.Fatal(err)
		}
		defer func(client *genai.Client) {
			err := client.Close()
			if err != nil {

			}
		}(client)

		model := client.GenerativeModel("gemini-pro")

		messagePrompt := strings.Join(messagePortions[1:], " ")

		fmt.Println(messagePrompt)
		resp, err := model.GenerateContent(ctx, genai.Text(messagePrompt))
		if err != nil {
			log.Fatal(err)
		}

		sendResponse(bot, message, resp)
	}
}

func sendResponse(bot *discordgo.Session, message *discordgo.MessageCreate, resp *genai.GenerateContentResponse) {
	for _, candidate := range resp.Candidates {
		if candidate.Content != nil {
			for _, part := range candidate.Content.Parts {
				_, err := bot.ChannelMessageSend(message.ChannelID, fmt.Sprintf("Response:\n%s", part))
				fmt.Println(part)
				if err != nil {
					return
				}
			}
		}
	}
}
